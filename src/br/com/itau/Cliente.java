package br.com.itau;

import java.util.Random;

public class Cliente {
    private int id;
    private String nome;
    private int idade;
    private String cpf;
    private TipoCliente tipoCliente;

    public Cliente() {
    }

    public Cliente(int id, String nome, int idade, String cpf, TipoCliente tipoCliente) throws Exception {
        if(idade < 18){
            throw new Exception("Idade precisa ser maior que 18");
        }

        this.id = id;
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
        this.tipoCliente = tipoCliente;
    }

    public Cliente novoCliente(String nome, int idade, String cpf, TipoCliente tipoCliente) throws Exception {

        Random random = new Random();
        return new Cliente(random.nextInt(5), nome, idade, cpf, tipoCliente);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", idade=" + idade +
                ", cpf='" + cpf + '\'' +
                '}';
    }

}
