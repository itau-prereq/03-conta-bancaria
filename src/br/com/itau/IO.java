package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static void imprimeMsgInicial(){
        System.out.println("Bem vindo");
    }

    public static void imprimeMsgInicial(String user){
        System.out.println("Bem vindo " + user);
    }

    public static void imprimeMsgOperacao(String operacao){

        System.out.println("Digite a quantia a ser depositada: ");
    }

    public static void imprimeSaldo(double valor){
        System.out.println("Saldo: " + valor);
    }

    //DTO - data transfer object
    public static Map<String, String> obterCliente(){
        Scanner scanner = new Scanner(System.in);

        Map<String, String> cliente = new HashMap<>();

        System.out.println("Dados do cliente ");
        System.out.println("Nome: ");
        String nome = scanner.nextLine();
        System.out.println("CPF: ");
        String cpf = scanner.nextLine();
        System.out.println("Idade: ");
        int idade = scanner.nextInt();

        cliente.put("nome", nome);
        cliente.put("cpf", cpf);
        cliente.put("idade", String.valueOf(idade));

        return cliente;
    }

    //DTO - data transfer object
    public static Map<String, String> obterConta(){
        Scanner scanner = new Scanner(System.in);

        Map<String, String> produto = new HashMap<>();

        System.out.println("Dados da conta ");
        System.out.println("Limite: ");
        String limite = scanner.nextLine();
        System.out.println("ID do cliente: ");
        String cliente = scanner.nextLine();

        produto.put("limite", limite);
        produto.put("cliente", cliente);

        return produto;
    }
}
