package br.com.itau;

import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        IO io = new IO();

        Conta conta = new Conta();
        Cliente cliente = new Cliente();

        boolean emUso = true;
        while(emUso) {
            System.out.println("Digite a opcao desejada: ");
            String operacao = scanner.nextLine();

            if(operacao == "criar conta") {
                conta = criarConta();
                System.out.println(conta.toString());
            }
            else if(operacao == "depositar"){
                    System.out.println("Digite a quantia a ser depositada: ");
                    String credito = scanner.nextLine();
                    conta.creditar(Double.parseDouble(credito));
                    System.out.println(conta.toString());
            }
            else if(operacao == "retirar"){
                    System.out.println("Digite a quantia a ser retirada: ");
                    String debito = scanner.nextLine();
                    conta.debitar(Double.parseDouble(debito));
                    System.out.println(conta.toString());
            }
            else if(operacao == "bloquear"){
                    conta.bloquearConta();
                    System.out.println(conta.toString());
            }
            else if(operacao == "desbloquear"){
                    conta.desbloquearConta();
                    System.out.println(conta.toString());
            }
            else if(operacao == "saldo"){
                    io.imprimeSaldo(conta.getSaldo());
            }
            else if(operacao == "criar cliente"){
                    cliente = criarCliente();
                    System.out.println(cliente.toString());
            }
            else if(operacao == "sair"){
                    emUso = false;
            }

            System.out.println(operacao == "sair");
        }

    }

    private static Cliente criarCliente() {
        IO io = new IO();
        Map<String, String> cliente =  io.obterCliente();
        Cliente c = new Cliente();
        try{
            c.novoCliente(cliente.get("nome"), Integer.parseInt(cliente.get("idade")), cliente.get("cpf"), TipoCliente.VAREJO);
        } catch (Exception e){
            System.out.println(e);
        }
        return c;
    }

    private static Conta criarConta(){
        IO io = new IO();
        Map<String, String> conta = io.obterConta();

        return Conta.novaConta(Integer.parseInt(conta.get("cliente")), Double.parseDouble(conta.get("limite")));
     }
}
