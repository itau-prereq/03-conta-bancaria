package br.com.itau;

import java.util.Random;

public class Conta {
    private int cliente;
    private int conta;
    private double saldo;
    private boolean bloqueio;
    private double limite;

    public static Conta novaConta(int cliente, double limite) {
//        this.conta = saldo;
        Random random = new Random();
        return new Conta(random.nextInt(5), cliente, 0.0, limite, false);
    }

    public Conta() {

    }

    public Conta(int conta, int cliente, double saldo, double limite, boolean bloqueio) {
        this.conta = conta;
        this.cliente = cliente;
        this.saldo = saldo;
        this.limite = limite;
        this.bloqueio = bloqueio;
    }

    public void creditar(double valor){

        this.saldo += valor;
    }

    public void debitar(double valor){

        this.saldo -= valor;
    }

    public void bloquearConta(){

        this.bloqueio = true;
    }

    public void desbloquearConta(){

        this.bloqueio = false;
    }

    public double getSaldo() {
        return saldo;
    }

    public double getLimite() {
        return limite;
    }

    public boolean isBloqueio() {
        return bloqueio;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    @Override
    public String toString() {
        return "Conta{" +
                "conta=" + conta +
                "cliente=" + cliente +
                ", saldo=" + saldo +
                ", bloqueio=" + bloqueio +
                ", limite=" + limite +
                '}';
    }

}
